# Record to change the polarity of the raster magnets
#
# Macros:
#  P
#  PS10kHz
#    40 = PS0
#    41 = PS1
#    42 = PS2
#  OUTPUT
#  ALTEVNT
#    EvtA = 10
#    EvtB = 11
#    ...
#    EvtH = 17

record(calcout, "$(P)#$(OUTPUT)-Alt-Pol-Calc") {
  field(CALC, "C")
  field(OUT,  "$(P)$(OUTPUT)-Src-SP PP")
  field(INPA, "62")
  field(INPB, "$(PS10kHz)")
  field(INPC, "61")
  field(INPD, "$(P)$(ALTEVNT)Cnt-I CPP")
  field(SCAN, "Passive")
}

record(mbbo, "$(P)#$(OUTPUT)-Pol-Sel") {
  field(DTYP, "Raw Soft Channel")
  field(ZRST, "High")
  field(ONST, "10 kHz")
  field(TWST, "Alternating")
  field(THST, "Random")
  field(FRST, "Disable")
  field(ZRVL, "0x1")
  field(ONVL, "0x2")
  field(TWVL, "0x4")
  field(THVL, "0x8")
  field(FRVL, "0x10")
  field(FVSV, "INVALID")
  field(SXSV, "INVALID")
  field(SVSV, "INVALID")
  field(EISV, "INVALID")
  field(NISV, "INVALID")
  field(TESV, "INVALID")
  field(ELSV, "INVALID")
  field(TVSV, "INVALID")
  field(TTSV, "INVALID")
  field(FTSV, "INVALID")
  field(FFSV, "INVALID")
  field(UNSV, "INVALID")
  field(VAL , "4")
  field(FLNK, "$(P)#$(OUTPUT)-Pol-Fanout PP NMS")
}

record(fanout, "$(P)#$(OUTPUT)-Pol-Fanout"){
  field(SELM, "Mask")
  field(SELL, "$(P)#$(OUTPUT)-Pol-Sel.RVAL NPP NMS")
  field(LNK1, "$(P)#$(OUTPUT)-Pol-High-StrOut PP NMS")
  field(LNK2, "$(P)#$(OUTPUT)-Pol-10kHz-StrOut PP NMS")
  field(LNK3, "$(P)#$(OUTPUT)-Pol-Alternating-StrOut PP NMS")
  field(LNK4, "$(P)#$(OUTPUT)-Pol-Random-StrOut PP NMS")
  field(LNK5, "$(P)#$(OUTPUT)-Pol-Disable-StrOut PP NMS")
}

record(stringout, "$(P)#$(OUTPUT)-Pol-High-StrOut") {
  field(VAL, "A")
  field(OUT, "$(P)#$(OUTPUT)-Alt-Pol-Calc.CALC PP")
}

record(stringout, "$(P)#$(OUTPUT)-Pol-10kHz-StrOut") {
  field(VAL, "B")
  field(OUT, "$(P)#$(OUTPUT)-Alt-Pol-Calc.CALC PP")
}

record(stringout, "$(P)#$(OUTPUT)-Pol-Alternating-StrOut") {
  field(VAL, "VAL=B?A:B")
  field(OUT, "$(P)#$(OUTPUT)-Alt-Pol-Calc.CALC NPP")
}

record(stringout, "$(P)#$(OUTPUT)-Pol-Random-StrOut") {
  field(VAL, "RNDM>0.5?A:B")
  field(OUT, "$(P)#$(OUTPUT)-Alt-Pol-Calc.CALC NPP")
}

record(stringout, "$(P)#$(OUTPUT)-Pol-Disable-StrOut") {
  field(VAL, "C")
  field(OUT, "$(P)#$(OUTPUT)-Alt-Pol-Calc.CALC PP")
}
